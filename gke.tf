// Configure the Google Cloud resources

resource "google_compute_network" "vpc_network" {
  name = "demo-network"
  auto_create_subnetworks = true 
  project = "learning-project-300210"
}

resource "google_container_cluster" "primary" {
  name                     = "demo-apps"
  location                 = "us-central1"
  remove_default_node_pool = true
  initial_node_count       = 1
  private_cluster_config {
    enable_private_nodes = true
    enable_private_endpoint = true
  }
  network = "vpc_network"
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool"
  location   = "us-central1"
  cluster    = google_container_cluster.primary.name
  node_count = 3

  node_config {
    preemptible  = true
    machine_type = "n1-standard-4"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

output "env-dynamic-url" {
  value = "https://${google_container_cluster.primary.endpoint}"
}
