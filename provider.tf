terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = ">=2.9.0"
      }
    google = {
      source = "hashicorp/google"
      version = "~> 2.5.0"
    }
  }
  backend "gcs" {
    bucket = "tfstate-demo-appsb"
    prefix = "dev"
    credentials = "learning-project.json"
  }  
}
provider "google" {
  credentials = file("learning-project.json")
  project     = "learning-project-300210"
  region      = "us-central1"
}
